package ru.remezov.animal;

public interface Running {

    default void canRun() {
        System.out.println("I can run!");
    }
}
