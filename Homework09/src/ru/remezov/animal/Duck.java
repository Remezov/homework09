package ru.remezov.animal;

public class Duck extends Animal implements Flyable, Running, Swimming{

    public Duck(String name) {
        super(name);
    }

    @Override
    public void getName() {
        System.out.println("I'm " +this.name+ "!");
    }


}
